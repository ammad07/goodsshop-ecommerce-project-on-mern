import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import Rating from './Rating';

const Product = ({product}) => {
  return (
    <>
    <Card className='my-3 rounded'>
            <Link to={`/product/${product._id}`} style={{'textDecoration': 'none'}}>
            <Card.Img variant="top" src={product.image} />
            <Card.Body>
                    <Card.Title as="div" className='my-3 p-2'> 
                        <strong>{product.name}</strong>
                    </Card.Title>
               
                <Card.Text as="div" className='my-3'>
                    <Rating value={product.rating} text={`${product.numReviews} reviews`}/>
                </Card.Text>
                <Card.Text as="h3" className='my-3'>$ {product.price}</Card.Text>
            </Card.Body>
            </Link>
    </Card>
    </>
  )
}

export default Product